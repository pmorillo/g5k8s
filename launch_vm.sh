#!/usr/bin/env bash
# Arguments : ipAddress macAddress vmName repoDirPath
set -e

. /grid5000/Modules/modules.sh
module load gvirt
TAP=$(sudo create_tap)
gvirt start $3 -i $4/gvirt/images/g5k8s.qcow2 --ip-address $1 --mac-address $2 --tap $TAP