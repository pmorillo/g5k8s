#!/usr/bin/env ruby

require 'json'

sshOptions = "-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no -o CheckHostIp=no"
repoDirPath = ARGV.first

def fork_exec(command, *args)
  # Remove empty args
  args.select! { |arg| arg != "" }
  args.flatten!
  pid = fork do
    Kernel.exec(command, *args)
  end
  #Process.wait(pid)
end

def sshCmd(host, command)
  sshOptions = "-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -o PasswordAuthentication=no -o CheckHostIp=no"
  puts "--> [#{host}] #{command}"
  puts %x[ssh #{sshOptions} root@#{host} #{command} 2>&1]
end

nodesList = %x[cat $OAR_NODEFILE | uniq].split("\n")
nodesCount = nodesList.count

subnetMap = Hash.new
%x[g5k-subnets -im | head -#{nodesCount}].split("\n").each.with_index do |x,i|
  commandResult = x.split("\t")
  subnetMap[nodesList[i]] = { :ip => commandResult.first, :mac => commandResult.last }
  if i == 0
    subnetMap[nodesList[i]][:name] = "kubemaster" if i == 0
    subnetMap[nodesList[i]][:type] = "master"
  else
    subnetMap[nodesList[i]][:name] = "kubenode#{i-1}" if i > 0
    subnetMap[nodesList[i]][:type] = "node"
  end
end

subnetMap.each do |host,args|
  puts "--> Start VM on #{host}"
  fork_exec("oarsh", host, repoDirPath + '/launch_vm.sh', args[:ip], args[:mac], args[:name], repoDirPath)
end

vmsDesc = {
  master: {},
  nodes: {}
}

subnetMap.each do |host,args|
  if args[:type] == "master"
    vmsDesc[:master][args[:name]] = args[:ip]
  else
    vmsDesc[:nodes][args[:name]] = args[:ip]
  end
end

open(repoDirPath + "/.customCache-#{ENV['OAR_JOBID']}.json", 'w') do |file|
  file.puts vmsDesc.to_json
end

sleep(86400)