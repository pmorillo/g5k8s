namespace :helm do

  desc 'Repositories config'
  task :repos do
    ip = clusterDesc()['master']['kubemaster']
    on(ip, :user => 'root') do
      [
        'helm repo add stable https://kubernetes-charts.storage.googleapis.com',
      ]
    end
  end

end