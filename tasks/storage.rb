namespace :storage do
  
  desc 'Generate RBD provisioner secrets and class'
  task :generate do
    # Genereate k8s secret YAML files
    entity = getCephAuth()
    %w{ ceph-admin-secret ceph-secret }.each do |sec|
      unless File.exists?("kubernetes/rbd-provisioner/#{sec}.yaml")
        secret = YAML.load_file("kubernetes/rbd-provisioner/#{sec}-template.yaml")
        secret['data']['key'] = Base64.encode64(entity['key'] + "\n").chomp
        File.open("kubernetes/rbd-provisioner/#{sec}.yaml", "w") do |file|
          file.puts secret.to_yaml
        end
        puts "** #{sec} YAML file generated : kubernetes/rbd-provisioner/#{sec}.yaml"
      else
        puts "** #{sec} YAML file already exists !"
      end
    end

    # Generate k8s storage class YAML file for RBD
    unless File.exists?("kubernetes/rbd-provisioner/class.yaml")
      monPort = XP5K::Config['site'] == 'rennes' ? '6790' : '6789'
      k8sClass = YAML.load_file("kubernetes/rbd-provisioner/class-template.yaml")
      k8sClass['parameters']['monitors'] = "ceph0.#{XP5K::Config['site']}.grid5000.fr:#{monPort}"
      k8sClass['parameters']['pool'] = XP5K::Config['user'] + '_g5k8s'
      k8sClass['parameters']['userId'] = XP5K::Config['user']
      k8sClass['parameters']['adminId'] = XP5K::Config['user']
      File.open("kubernetes/rbd-provisioner/class.yaml", "w") do |file|
        file.puts k8sClass.to_yaml
      end
      puts "** rbd Storage Class YAML file generates : kubernetes/rbd-provisioner/class.yaml"
    else
      puts "** rbd Storage Class YAML file already exists !"
    end
  end
  
  desc 'Manage Ceph pool for kubernetes'
  task :managePools do
    pools = getCephPools()
    kubePool = pools.select { |x| x['pool_name'] == XP5K::Config['user'] + '_g5k8s' }
    if kubePool.count == 0
      puts "Creating Ceph pool for Kubernetes..."
      createCephPool
    else
      puts "Ceph pool for Kubernetes already exists !"
    end
  end

  desc 'Deploy rbd provisioner'
  task :deployprovisioner do
    ip = clusterDesc()['master']['kubemaster']
    on(ip, :user => 'root') do
      [
        "git clone https://github.com/kubernetes-incubator/external-storage.git",
        "kubectl create -f external-storage/ceph/rbd/deploy/rbac/"
      ]
    end
  end

  desc 'Deploy rbd provisioner'
  task :deploy do
    workflow = [
      'storage:managePools',
      'storage:deployprovisioner',
      'storage:generate',
      'storage:createRbdClass'
    ]
    workflow.each do |task|
      Rake::Task[task].execute
    end
  end

  desc 'Create secrets and storage class'
  task :createRbdClass do
    ip = clusterDesc()['master']['kubemaster']
    repoLocalPath = Dir.pwd
    repoVmPath = repoLocalPath.sub(ENV['HOME'], "/mnt/home")
    on(ip, :user => 'root') do
      [
        "kubectl create -f #{repoVmPath}/kubernetes/rbd-provisioner/ceph-secret.yaml",
        "kubectl create -f #{repoVmPath}/kubernetes/rbd-provisioner/ceph-admin-secret.yaml",
        "kubectl create -f #{repoVmPath}/kubernetes/rbd-provisioner/class.yaml",
        %{kubectl patch storageclass rbd -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"true"}}}'}
      ]
    end
  end
  
  def cephApiBaseUrl()
    return "https://api.grid5000.fr/sid/sites/#{XP5K::Config['site']}/storage/ceph/"
  end

  def getCephAuth()
    resp = Net::HTTP.get URI(cephApiBaseUrl + "auths/" + XP5K::Config['user'])
    entity = JSON.parse(resp)
  end
  
  def getCephPools()
    resp = Net::HTTP.get URI(cephApiBaseUrl + "pools")
    pools = JSON.parse(resp)
  end
  
  def createCephPool()
    expirationDate = (Time.now + (60 * 60 * 24 * 28 * 2)).strftime("%Y-%m-%d")
    pool = {
      "poolName" => "g5k8s",
      "size" => 3,
      "quota" => 200000000000, # 200GB
      "expiration_date" => expirationDate
    }
    uri = URI(cephApiBaseUrl + "pools")
    req = Net::HTTP::Post.new(uri)
    req.set_form_data(pool)
    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => true) do |http|
      http.request(req)
    end
    
    case res
    when Net::HTTPSuccess, Net::HTTPRedirection
      puts res.body
    else
      p res.body
      res.value
    end
  end
  
end