namespace :vm do

  desc 'Wait for VMs booting and final description'
  task :wait do
    until File.exists?(".customCache-#{jobID()}.json") do
      sleep(1)
    end
    ip = clusterDesc()['master']['kubemaster']
    until testSSH(ip) do
      puts "VM not ready... (sleep 5s)"
      sleep(5)
    end
  end

  desc 'Configure ssh'
  task :ssh do
    ips = []
    clusterDesc().each do |_,kind|
      kind.each do |_,ip|
        ips << ip
      end
    end
    on(ips, :user => 'root') do
      %{cat /root/.ssh/id_rsa.pub >> /root/.ssh/authorized_keys}
    end
  end

end

def clusterDesc
  file = File.read(".customCache-#{jobID()}.json")
  JSON.parse(file)
end

def updateDesc(newDesc)
  File.open(".customCache-#{jobID()}.json", 'w') do |file|
    file.puts newDesc
  end
end

def testSSH(host)
  Timeout.timeout(5) do
    TCPSocket.new(host, 22).close
  end
  true
rescue Errno::ECONNREFUSED
  true
rescue StandardError, Timeout::StandardError
  false
end