require 'erb'

namespace :kube do

  task :setup do
    if not File.exists?('./gvirt/images/g5k8s.qcow2')
      puts "You need to generate an image before."
      exit 0
    end
  end

  desc 'Deploy metallb'
  task :deployMetallb do
    subnet = xp.job_with_name(XP5K::Config[:jobname])['resources_by_type']['subnets'].first
    ipaddr = IPAddr.new(subnet)
    range = ipaddr.to_range.to_a[257..510]
    rangeStr = "#{range.first.to_s}-#{range.last.to_s}"
    tmpl = ERB.new(File.read('kubernetes/metallb/cm-metallb.yaml.erb'))
    cm = tmpl.result(binding)
    File.open("kubernetes/metallb/cm-metallb.yaml", 'w') do |file|
      file.puts cm
    end
    ip = clusterDesc()['master']['kubemaster']
    repoLocalPath = Dir.pwd
    repoVmPath = repoLocalPath.sub(ENV['HOME'], "/mnt/home")
    on(ip, :user => 'root') do
      [
        'kubectl create ns metallb',
        "kubectl -n metallb apply -f #{repoVmPath}/kubernetes/metallb/cm-metallb.yaml",
        %{helm install metallb --namespace metallb stable/metallb}
      ]
    end
  end

end
