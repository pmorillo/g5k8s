namespace :rke do
  
  desc 'Generate RKE cluster file'
  task :generate do
    FileUtils.mkdir_p('generated/rke') unless File.exists?('generated/rke')
    config = YAML.load(File.read('kubernetes/rke/cluster.yml'))
    node_template = config['nodes'].first
    generated_nodes = []

    clusterDesc()['master'].each do |node, ip|
      node_config = Marshal.load(Marshal.dump(node_template))
      node_config['address'] = ip
      node_config['role'] = ['controlplane', 'etcd'] 
      node_config['hostname_override'] = node
      generated_nodes << node_config
    end

    clusterDesc()['nodes'].each do |node, ip|
      node_config = Marshal.load(Marshal.dump(node_template))
      node_config['address'] = ip
      node_config['role'] = ['worker'] 
      node_config['hostname_override'] = node
      generated_nodes << node_config
    end

    config['nodes'] = generated_nodes

    File.open('./generated/rke/cluster.yml', 'w') do |file|
      file.puts config.to_yaml
    end
  end

  desc 'Running RKE cluster'
  task :up do
    ip = clusterDesc()['master']['kubemaster']
    repoLocalPath = Dir.pwd
    repoVmPath = repoLocalPath.sub(ENV['HOME'], "/mnt/home")
    on(ip, :user => 'root') do
      "cd #{repoVmPath}/generated/rke && rke up"
    end
  end

  desc 'Configue kubectl on kubemaster'
  task :kubeconfig do
    ip = clusterDesc()['master']['kubemaster']
    repoLocalPath = Dir.pwd
    repoVmPath = repoLocalPath.sub(ENV['HOME'], "/mnt/home")
    on(ip, :user => 'root') do
      [
        "mkdir /root/.kube",
        "cp #{repoVmPath}/generated/rke/kube_config_cluster.yml /root/.kube/config",
      ]
    end
  end

  desc 'Deploy RKE cluster'
  task :deploy do
    workflow = [
      'rke:generate',
      'rke:up',
      'rke:kubeconfig'
    ]
    workflow.each do |task|
      Rake::Task[task].execute
    end
  end

end