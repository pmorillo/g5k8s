namespace :monitoring do

  namespace :helm do

    desc 'Generate Helm values'
    task :generate do
      FileUtils.mkdir_p('generated/monitoring/prometheus-operator/helm') unless File.exists?('generated/monitoring/prometheus-operator/helm')
      values = YAML.load(File.read('namespaces/monitoring/prometheus-operator/helm/values.yaml'))
      worker_ip = clusterDesc()['nodes']['kubenode0']

      values['alertmanager']['ingress']['hosts'] = ["g5k-alertmanager.#{worker_ip}.xip.io"]
      values['prometheus']['ingress']['hosts'] = ["g5k-prometheus.#{worker_ip}.xip.io"]
      values['grafana']['ingress']['hosts'] = ["g5k-grafana.#{worker_ip}.xip.io"]

      File.open('generated/monitoring/prometheus-operator/helm/values.yaml', 'w') do |file|
        file.puts values.to_yaml
      end
    end

  end

  desc 'Deploy Monitoring'
  task :deployPrometheus do
    ip = clusterDesc()['master']['kubemaster']
    localpath = Dir.pwd
    vmpath = localpath.gsub("/home/#{ENV['USER']}", "/mnt/home")
    on(ip, :user => 'root') do
      [
        "kubectl create ns monitoring",
        "helm install prom-op --namespace monitoring -f #{vmpath}/generated/monitoring/prometheus-operator/helm/values.yaml --version=8.3.3 stable/prometheus-operator",
        "kubectl -n monitoring rollout status deploy/prom-op-prometheus-operato-operator --watch"
      ]
    end
  end

  task :deploy do
    workflow = [
      'monitoring:helm:generate',
      'monitoring:deployPrometheus'
    ]
    workflow.each do |task|
      Rake::Task[task].execute
    end
  end

end