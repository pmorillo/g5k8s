namespace :snapshot do
  
  desc 'Create a cluster snapshot (name=)'
  task :create do
    raise 'Need arg name=' unless snapname = ENV['name']
    if File.exists?("snapshots/#{snapname}")
      puts "=== Snapshot #{snapname} already exists !"
      exit 0
    end 
    
    FileUtils.mkdir_p("snapshots/#{snapname}")
    sh "cp generated/rke/cluster.rkestate snapshots/#{snapname}/"
    sh "cp generated/rke/cluster.yml snapshots/#{snapname}/"
    sh "cp generated/rke/kube_config_cluster.yml snapshots/#{snapname}/"

    ip = clusterDesc()['master']['kubemaster']
    localpath = Dir.pwd
    vmpath = localpath.gsub("/home/#{ENV['USER']}", "/mnt/home")
    on(ip, :user => 'root') do
      [
        "rke etcd snapshot-save --config #{vmpath}/generated/rke/cluster.yml --name rke_snapshot",
        "cp /opt/rke/etcd-snapshots/rke_snapshot.zip #{vmpath}/snapshots/#{snapname}"
      ]
    end
  end

  desc "Restore a cluster snapshot (name=)"
  task :restore do
    
  end

end