# Rakefile
# Rakefile for Kubernetes deployment on Grid'5000

require 'bundler'
Bundler.setup

require 'xp5k'
require 'xp5k/rake'
require 'ipaddr'

# Load ./xp.conf file
#
XP5K::Config.load

# Constants
#
SSH_CONFIGFILE_OPT = XP5K::Config[:ssh_config].nil? ? '' : ' -F ' + XP5K::Config[:ssh_config]

# Initialize experiment
#
@xp = XP5K::XP.new
def xp; @xp; end

# Defaults configuration
#
XP5K::Config[:walltime]       ||= '1:00:00'               # OAR job duration
XP5K::Config[:user]           ||= ENV['USER']             # Grid'5000 user login (useful from an external machine with a different login name)
XP5K::Config[:clusterSize]    ||= 3                       # Master + Nodes
XP5K::Config[:jobname]        ||= 'g5K8s'                 # OAR job name
XP5K::Config[:site]           ||= 'rennes'                # Default site (Work only at Rennes and Nantes)
XP5K::Config[:nodeSelection]  ||= ''                      # OAR properties selection (ex: "{cluster in ('paravance', 'parasilo')}")
XP5K::Config[:onRetry]        = true
XP5K::Config[:onRetryDelay]   = 1
XP5K::Config[:pluggedProject] ||= nil

# Load Rake tasks
#
Dir['tasks/*.rb'].each do |taskfile|
  load taskfile
end

# Load plugged projects
if XP5K::Config[:pluggedProject]
  Dir["#{XP5K::Config[:pluggedProject]}/tasks/*.rb"].each do |taskfile|
    load taskfile
  end
end

desc 'Run metatask'
task :run do
  workflow = [
    'kube:setup',
    'grid5000:jobs',
    'vm:wait',
    'vm:ssh',
    'rke:deploy',
    'helm:repos',
    'kube:deployMetallb'
  ]
  workflow.each do |task|
    Rake::Task[task].execute
  end
  XP5K::Rake::Timer.summary
end

# Fork the execution of a command. Used to execute ssh on k8s master nodes.
def fork_exec(command, *args)
  # Remove empty args
  args.select! { |arg| arg != "" }
  args.flatten!
  pid = fork do
    Kernel.exec(command, *args)
  end
  Process.wait(pid)
end

desc 'Open a shell on the kubernetes master'
task :shell do
  fork_exec('ssh', SSH_CONFIGFILE_OPT.split(" "), 'root@' + clusterDesc()['master']['kubemaster'])
end
