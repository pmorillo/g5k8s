## Kubernetes Prometheus operator

### Deployment

```sh
rake monitoring:deploy
#...
# deployment "prom-op-prometheus-operato-operator" successfully rolled out
```

Check the deployment :

```sh
rake shell
root@kubemaster:~# kubectl get pods -n monitoring -o wide
NAME                                                   READY     STATUS              RESTARTS   AGE       IP           NODE         NOMINATED NODE
alertmanager-kube-prometheus-0                         2/2       Running             0          33s       10.244.4.3   kubenode2    <none>
kube-prometheus-exporter-kube-state-658f46b8dd-zt542   1/2       Running             0          34s       10.244.1.3   kubenode0    <none>
kube-prometheus-exporter-kube-state-69d85f857b-cv95h   0/2       ContainerCreating   0          15s       <none>       kubenode3    <none>
kube-prometheus-exporter-node-4xrwn                    1/1       Running             0          33s       10.158.0.1   kubemaster   <none>
kube-prometheus-exporter-node-gpfxq                    1/1       Running             0          34s       10.158.0.2   kubenode0    <none>
kube-prometheus-exporter-node-h4s5g                    1/1       Running             0          34s       10.158.0.5   kubenode3    <none>
kube-prometheus-exporter-node-h7w2c                    1/1       Running             0          33s       10.158.0.4   kubenode2    <none>
kube-prometheus-exporter-node-t4rqs                    1/1       Running             0          34s       10.158.0.3   kubenode1    <none>
kube-prometheus-grafana-6596c455c8-lzpxd               0/2       ContainerCreating   0          33s       <none>       kubenode1    <none>
prometheus-kube-prometheus-0                           0/3       ContainerCreating   0          33s       <none>       kubenode1    <none>
prometheus-operator-858c485-xtx5x                      1/1       Running             0          1m        10.244.3.2   kubenode3    <none>
```

Access to web applications :

```sh
root@kubemaster:~# kubectl -n monitoring get ingress
# NAME                                      HOSTS                                ADDRESS                                       PORTS   AGE
# prom-op-grafana                           g5k-grafana.10.158.0.2.xip.io        10.158.0.2,10.158.0.3,10.158.0.4,10.158.0.5   80      3m23s
# prom-op-prometheus-operato-alertmanager   g5k-alertmanager.10.158.0.2.xip.io   10.158.0.2,10.158.0.3,10.158.0.4,10.158.0.5   80      3m23s
# prom-op-prometheus-operato-prometheus     g5k-prometheus.10.158.0.2.xip.io     10.158.0.2,10.158.0.3,10.158.0.4,10.158.0.5   80      3m23s
```

### Grafana

Browse : [g5k-grafana.10.158.0.2.xip.io](http://g5k-grafana.10.158.0.2.xip.io) (admin/g5k)

!!! Tip
    Use [Grid'5000 VPN](https://www.grid5000.fr/mediawiki/index.php/VPN) or [socks proxy](https://www.grid5000.fr/mediawiki/index.php/SSH#Using_OpenSSH_SOCKS_proxy) for external access.


![Grafana Screenshot](img/grafana_screenshot.png)

