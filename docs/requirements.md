* OAR resources : nodes (>=3) + 1 subnets.
* gvirt (Qemu KVM wrapper, only availble at Rennes).
* Ceph for dynamic persistent volumes (Optional, only available at Rennes and Nantes).

## Initial setup

### Download Gitlab project

From a Grid'5000 frontend :

```sh
ssh frennes.rennes.grid5000.fr
git clone https://gitlab.inria.fr/pmorillo/g5k8s.git
cd g5k8s
```

### Ruby environment

```sh
source setup_env.sh
gem install bundler
bundle install
```

### Build qemu KVM image (~3mn)

```sh
oarsub -S ./gvirt/generate_image.sh
```

This OAR job will create a virtual image based on Debian 10 with :

* Kubectl 1.17.2 
* RKE 1.0.3 and pre-pulled k8s 1.17.2 images.
* [Helm](http://helm.sh) 3.0.2.

At the end of this OAR job, the image will be available at `./gvirt/images/g5k8s.qcow2`.
