## Configuration

```sh
cp xp.conf.template xp.conf
```

!!! tip "`xp.conf` file"
    * To customize the cluster size (default : 3), add :

    ```
    clusterSize     5 # 1 master, 4 nodes
    ```

    * To select specific OAR resources, add :

    ```
    nodeSelection   "{cluster IN ('parasilo','paravance')}"
    ```

## Deployment (~4mn)

```sh
source setup_env.sh
rake run
#...
# |-- run : 00h 03m 12s
# |   |-- kube:setup -> 00h 00m 00s
# |   |-- grid5000:jobs -> 00h 00m 03s
# |   |-- vm:wait -> 00h 00m 00s
# |   |-- vm:ssh -> 00h 00m 00s
# |   |-- rke:deploy -> 00h 02m 57s
# |   |   |-- rke:generate -> 00h 00m 00s
# |   |   |-- rke:up -> 00h 02m 56s
# |   |   |-- rke:kubeconfig -> 00h 00m 00s
# |   |-- helm:repos -> 00h 00m 07s
# |   |-- kube:deployMetallb -> 00h 00m 03s
```

## Open a shell on the master node

```sh
rake shell
```

## Inspect deployment

### Cluster informations

```sh
root@kubemaster:~# kubectl cluster-info
# Kubernetes master is running at https://10.158.0.1:6443
# KubeDNS is running at https://10.158.0.1:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

### Components statuses

```sh
root@kubemaster:~# kubectl get cs
# NAME                 STATUS    MESSAGE              ERROR
# scheduler            Healthy   ok
# controller-manager   Healthy   ok
# etcd-0               Healthy   {"health": "true"}
```

!!! Tip
    `cs` shortname for `componentstatuses`. Use command `kubectl api-resources` to shows available resources and shortnames.

### Cluster nodes

```sh
root@kubemaster:~# kubectl get nodes -o wide
# NAME         STATUS   ROLES    AGE     VERSION   INTERNAL-IP   EXTERNAL-IP   OS-IMAGE                       KERNEL-VERSION   CONTAINER-RUNTIME
# kubemaster   Ready    controlplane,etcd   48m   v1.17.2   10.158.0.1    <none>        Debian GNU/Linux 10 (buster)   4.19.0-8-amd64   docker://19.3.5
# kubenode0    Ready    worker              48m   v1.17.2   10.158.0.2    <none>        Debian GNU/Linux 10 (buster)   4.19.0-8-amd64   docker://19.3.5
# kubenode1    Ready    worker              48m   v1.17.2   10.158.0.3    <none>        Debian GNU/Linux 10 (buster)   4.19.0-8-amd64   docker://19.3.5
# kubenode2    Ready    worker              48m   v1.17.2   10.158.0.4    <none>        Debian GNU/Linux 10 (buster)   4.19.0-8-amd64   docker://19.3.5
# kubenode3    Ready    worker              48m   v1.17.2   10.158.0.5    <none>        Debian GNU/Linux 10 (buster)   4.19.0-8-amd64   docker://19.3.5
```

### Pods

```sh
root@kubemaster:~# kubectl get pods --all-namespaces -o wide
# NAMESPACE     NAME                                 READY   STATUS    RESTARTS   AGE     IP           NODE         NOMINATED NODE   READINESS GATES
# ingress-nginx   default-http-backend-67cf578fc4-xj6ks                    1/1     Running     0          6m52s   10.46.1.5    kubenode0    <none>           <none>
# ingress-nginx   nginx-ingress-controller-8tlrl                           1/1     Running     0          6m52s   10.158.0.3   kubenode1    <none>           <none>
# ingress-nginx   nginx-ingress-controller-dlnkh                           1/1     Running     0          6m52s   10.158.0.4   kubenode2    <none>           <none>
# ingress-nginx   nginx-ingress-controller-kmrls                           1/1     Running     0          6m52s   10.158.0.2   kubenode0    <none>           <none>
# ingress-nginx   nginx-ingress-controller-qhldr                           1/1     Running     0          6m52s   10.158.0.5   kubenode3    <none>           <none>
# kube-system     canal-94wxx                                              2/2     Running     0          7m10s   10.158.0.4   kubenode2    <none>           <none>
# kube-system     canal-gz2sp                                              2/2     Running     0          7m10s   10.158.0.1   kubemaster   <none>           <none>
# kube-system     canal-jr7vk                                              2/2     Running     0          7m10s   10.158.0.2   kubenode0    <none>           <none>
# kube-system     canal-r85jv                                              2/2     Running     0          7m10s   10.158.0.3   kubenode1    <none>           <none>
# kube-system     canal-vh62d                                              2/2     Running     0          7m10s   10.158.0.5   kubenode3    <none>           <none>
# kube-system     coredns-7c5566588d-tbtn7                                 1/1     Running     0          7m3s    10.46.1.3    kubenode0    <none>           <none>
# kube-system     coredns-7c5566588d-z87h6                                 1/1     Running     0          6m56s   10.46.4.2    kubenode2    <none>           <none>
# kube-system     coredns-autoscaler-65bfc8d47d-gxbcq                      1/1     Running     0          7m2s    10.46.1.2    kubenode0    <none>           <none>
# kube-system     metrics-server-6b55c64f86-zpnxf                          1/1     Running     0          6m56s   10.46.1.4    kubenode0    <none>           <none>
# kube-system     rke-coredns-addon-deploy-job-4xdnh                       0/1     Completed   0          7m5s    10.158.0.1   kubemaster   <none>           <none>
# kube-system     rke-ingress-controller-deploy-job-nkdrc                  0/1     Completed   0          6m53s   10.158.0.1   kubemaster   <none>           <none>
# kube-system     rke-metrics-addon-deploy-job-wcbgk                       0/1     Completed   0          6m58s   10.158.0.1   kubemaster   <none>           <none>
# kube-system     rke-network-plugin-deploy-job-b99qq                      0/1     Completed   0          7m12s   10.158.0.1   kubemaster   <none>           <none>
# metallb         metallb-controller-7fbbd7dcbb-5v5ft                      1/1     Running     0          5m34s   10.46.1.6    kubenode0    <none>           <none>
# metallb         metallb-speaker-bwrl4                                    1/1     Running     0          5m34s   10.158.0.3   kubenode1    <none>           <none>
# metallb         metallb-speaker-dms9c                                    1/1     Running     0          5m34s   10.158.0.4   kubenode2    <none>           <none>
# metallb         metallb-speaker-kxx96                                    1/1     Running     0          5m34s   10.158.0.2   kubenode0    <none>           <none>
# metallb         metallb-speaker-zhmfp                                    1/1     Running     0          5m34s   10.158.0.5   kubenode3    <none>           <none>
```