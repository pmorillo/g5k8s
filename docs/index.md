!!! warning
    Not maintained anymore, prefer to use [Terraform Grid5000 provider](https://github.com/pmorillon/terraform-provider-grid5000/tree/master/examples/kubernetes).

G5K8s is a tool to deploy a Kubernetes cluster on [Grid'5000](https://www.grid5000.fr) in less than 5 minutes.

Features :

* Deploy a kubernetes cluster (using [Rancher Kubernetes Engine (RKE)](https://rancher.com/docs/rke/latest/en/)).
* Install and configure the [Helm](https://helm.sh) package manager for Kubernetes.
* Monitoring with [CoreOS prometheus-operator](https://github.com/coreos/prometheus-operator).
* Deploy [Metallb](https://metallb.universe.tf) network load balancer.
* Support for dynamic persistent volumes (Ceph RBD, only available at Rennes and Nantes).