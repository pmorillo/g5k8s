#!/usr/bin/env bash
set -ex

DOCKER_VERSION="19.03.5"
RKE_VERSION="1.0.3"
HELM_VERSION="3.0.2"
KUBECTL_VERSION="1.17.2"

apt update

# Requirements
#
apt-get install -y apt-transport-https ca-certificates curl software-properties-common gnupg wget ceph-common


# Use iptables legacy (Only for Debian 10)
#
update-alternatives --set iptables /usr/sbin/iptables-legacy
update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy

# Install Docker
#
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb https://download.docker.com/linux/$(. /etc/os-release; echo "$ID") $(lsb_release -cs) stable"
apt update
PKG_VERSION=$(apt-cache madison docker-ce | grep ${DOCKER_VERSION} | head -1 | awk '{print $3}')
cat << EOF > /etc/apt/preferences.d/docker.pref
# Pinning to prevent docker upgrades

Package: docker-ce
Pin: version ${PKG_VERSION}
Pin-Priority: 1001
EOF
apt-get install -y docker-ce


# Install RKE
#
wget -O /usr/local/bin/rke https://github.com/rancher/rke/releases/download/v${RKE_VERSION}/rke_linux-amd64
chmod a+x /usr/local/bin/rke

# Disable swp
swapoff -a
sed -i '/swap/d' /etc/fstab

# Pull docker images
for i in $(rke config -s | tail -n+2); do echo "=== Pull image $i"; docker pull $i; done

# Install Helm
wget https://get.helm.sh/helm-v${HELM_VERSION}-linux-amd64.tar.gz
tar xf helm-v${HELM_VERSION}-linux-amd64.tar.gz
mv linux-amd64/helm /usr/local/bin/

# Install kubectl
curl -LO https://storage.googleapis.com/kubernetes-release/release/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl
chmod a+x kubectl
mv kubectl /usr/local/bin

# Load rbd module at boot time
echo "rbd" >> /etc/modules

# Clean APT caches and shutdown the VM
apt-get clean
sync && sync && sync
shutdown now
